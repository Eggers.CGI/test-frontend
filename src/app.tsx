import './app.css'

export function App() {
  return (
    <div className="flex justify-center">
      <h1 className="font-bold text-2xl text-blue-900">
        preact and Tailwind with Vitejs!
      </h1>
    </div>
  );
}